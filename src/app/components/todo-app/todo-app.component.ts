import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { Todo } from './../../model/Todo';

@Component({
  selector: 'todo-app',
  templateUrl: './todo-app.component.html',
  styleUrls: ['./todo-app.component.css']
})
export class TodoAppComponent {

  todos : Todo[];
  inputTodo:string;
  hideAddButton:boolean = false ;
  itemId : number ;
  hideAlert:boolean=false;
  HomeIsLoading:boolean=false;

  constructor(public _router:Router) { }

  ngOnInit(): void {

    this.todos=[
      {
        content:'first task',
        done : false
    },
    {
      content:'second task',
      done : false
  }]
  }
  
  toggleShowSaveBtn() {
    this.hideAddButton=true;
  }

  addTodo (alert:HTMLDivElement){
    if(this.inputTodo.trim() =='' || this.inputTodo.trim()  == undefined){
      alert.removeAttribute('style');
    }
    else{
    this.todos.push({
      content : this.inputTodo ,
      done:false 
    });
    alert.setAttribute('style','display:none');
  }
    this.inputTodo="";
  }

  deleteTodo(taskid:number){
    this.todos = this.todos.filter((v,id)=> id !== taskid);
      this.hideAddButton = false;
      this.inputTodo="";
  }

  editing(id:number , todo:string , input:HTMLInputElement ){
    this.inputTodo = todo;
    this.itemId=id;
    input.focus();
  }

  updating(){
    this.hideAddButton=false;
     this.todos[this.itemId].content=this.inputTodo;
     this.inputTodo="";
  }
 
  checked(btnE:HTMLButtonElement, checkBox:HTMLInputElement){
    if(checkBox.checked)
    btnE.setAttribute('style' , 'display:none');
    else
    btnE.removeAttribute('style');
  }

  toggleHomeLoading(){
    this.HomeIsLoading=true;
    setTimeout(()=>{
      this.HomeIsLoading=false;
      this._router.navigateByUrl('/home');
    },1000);
   }

}
