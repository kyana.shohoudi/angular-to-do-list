import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  myimage:string ="assets/images/To-Do-List.png/";  
  TaskIsLoading:boolean=false;

  constructor(public _router:Router ) { }

  ngOnInit(): void {
   }
   
   toggleTaskLoading(){
    this.TaskIsLoading=true;
    setTimeout(()=>{
      this.TaskIsLoading=false;
      this._router.navigateByUrl('/todo-app');
    },1000);
   }
}

