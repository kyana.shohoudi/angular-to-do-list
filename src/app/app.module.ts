import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { TodoAppComponent } from './components/todo-app/todo-app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import {RouterModule, Routes} from "@angular/router";

const appRoutes: Routes = [
  {path: '' , component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'todo-app', component: TodoAppComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    TodoAppComponent,
    HomeComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }